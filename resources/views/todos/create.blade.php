<h1>Create a new todo</h1>

<form action="{{action('TodoController@store')}}" method='post'>
    {{ csrf_field()}}

    <div class = "form-group">
        <label for="titale">What would you like todo?</label>
        <input type="text" class = "form-control" name = "titale">
    </div>

    <div class = "form-group">
        <input type="submit" class = "form-control" name = "submit" value = "Save">
    </div>

</form>
